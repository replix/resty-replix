module gitlab.com/replix/resty-replix

go 1.15

require (
	github.com/go-resty/resty/v2 v2.5.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/google/uuid v1.1.4
	github.com/stretchr/testify v1.6.1
)
