package replix

import (
	"errors"
	"net/http"
	"time"

	v20201231 "gitlab.com/replix/resty-replix/pkg/v20201231"
)

const deleteVolumeCheckDelay = time.Second * 10
const setPrimaryCheckDelay = time.Second * 2
const waitVolumeCheckDelay = time.Second * 5

// DeleteVolumeAndWait - deletes a volume and waits until the process is over
func (c *Client) DeleteVolumeAndWait(volumeName string) error {
	_, err := c.DeleteVolume(volumeName)
	_, err = c.waitForVolumeImpl(volumeName, deleteVolumeCheckDelay)

	var fault *v20201231.Fault
	if errors.As(err, &fault) {
		if fault.HTTPCode == http.StatusNotFound {
			return nil
		}
	}
	return err

}

// SetPrimaryAndWait - sets new primary replica and waits untils the process is over
func (c *Client) SetPrimaryAndWait(volumeName string, primaryReplica string) (v20201231.Replica, error) {
	replica, err := c.SetPrimary(volumeName, primaryReplica)
	if replica.Primary && replica.Status.Value == v20201231.Online {
		return replica, nil
	}

	var volume v20201231.Volume

	for err == nil {
		volume, err = c.GetVolume(volumeName)
		r, _ := volume.Replica(primaryReplica)
		if volume.Status.Value == v20201231.Online && r.Status.Value == v20201231.Online && r.Primary {
			return *r, nil
		}
		time.Sleep(setPrimaryCheckDelay)
	}

	return replica, err
}

// WaitForVolume - wait for volume to reach a steady state
func (c *Client) WaitForVolume(volumeName string) (v20201231.Volume, error) {
	return c.waitForVolumeImpl(volumeName, waitVolumeCheckDelay)
}

func (c *Client) waitForVolumeImpl(volumeName string, delay time.Duration) (v20201231.Volume, error) {
	var volume v20201231.Volume
	var err error
	for err == nil {
		volume, err = c.GetVolume(volumeName)
		if volume.Status.IsSteady() {
			break
		}
		time.Sleep(delay)
	}
	return volume, err
}
