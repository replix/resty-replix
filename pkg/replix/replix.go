package replix

import (
	"encoding/json"
	"strings"

	v20201231 "gitlab.com/replix/resty-replix/pkg/v20201231"

	resty "github.com/go-resty/resty/v2"
)

// GetVolumes - get all volumes info
// GetSingleVolume - specific volume info
// GetVolumeStatus - get the volume info and return its status
// GetReplicaStatus- get the volume info and return a spicified replica status
// CreateVolume - create a new volume
// SetPrimaryVolumeReplica - set the specified volume replica as primary
// DeleteVolume - delete an existing volume

// GET descriptors of existing objects - Responses

// Client - client
type Client struct {
	client *resty.Client
}

// New - constructor of client
func New(url string, token ...string) Client {
	client := resty.New().SetHostURL(url)

	for _, token := range token {
		client.SetAuthToken(token)
	}

	return Client{
		client,
	}
}

// SetAuthToken - set new REST API Auth Token
func (c *Client) SetAuthToken(authToken string) {
	c.client.SetAuthToken(authToken)
}

func (c *Client) url(path string) string {
	return "/" + v20201231.APIVersionPrefix + path
}

func (c *Client) get(path string, query ...string) (*resty.Response, error) {
	queryString := strings.Join(query, "&")
	return c.client.R().
		SetQueryString(queryString).
		SetHeader("Accept", "application/json").
		Get(path)
}

func (c *Client) delete(path string, query ...string) (*resty.Response, error) {
	queryString := strings.Join(query, "&")
	return c.client.R().
		SetQueryString(queryString).
		SetHeader("Accept", "application/json").
		Delete(path)
}

func (c *Client) post(path string, body interface{}, query ...string) (*resty.Response, error) {
	queryString := strings.Join(query, "&")
	return c.client.R().
		SetQueryString(queryString).
		SetHeader("Accept", "application/json").
		SetBody(body).
		Post(path)
}

func (c *Client) put(path string, query ...string) (*resty.Response, error) {
	queryString := strings.Join(query, "&")
	return c.client.R().
		SetQueryString(queryString).
		SetHeader("Accept", "application/json").
		Put(path)
}

// GetRealms - get all realms
func (c *Client) GetRealms() ([]v20201231.Realm, error) {

	path := c.url("/realms")

	var fault v20201231.Fault
	realms := []v20201231.Realm{}

	response, err := c.get(path)

	if err != nil {
		return realms, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &realms)
		return realms, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return realms, &fault
	}

	return realms, err
}

// GetRealm - get a specific realm
func (c *Client) GetRealm(name string) (v20201231.Realm, error) {
	path := c.url("/realms/" + name)

	var fault v20201231.Fault
	realm := v20201231.Realm{}

	response, err := c.get(path)

	if err != nil {
		return realm, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &realm)
		return realm, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return realm, &fault
	}

	return realm, err
}

// GetVolumes - get all volumes
func (c *Client) GetVolumes() ([]v20201231.Volume, error) {

	path := c.url("/volumes")

	var fault v20201231.Fault
	volumes := []v20201231.Volume{}

	response, err := c.get(path)

	if err != nil {
		return volumes, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &volumes)
		return volumes, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return volumes, &fault
	}

	return volumes, err
}

// GetVolume - get a specific volume
func (c *Client) GetVolume(name string) (v20201231.Volume, error) {
	path := c.url("/volumes/" + name)

	var fault v20201231.Fault
	volume := v20201231.Volume{}

	response, err := c.get(path)

	if err != nil {
		return volume, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &volume)
		return volume, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return volume, &fault
	}

	return volume, err
}

// GetReplica - get a specific volume
func (c *Client) GetReplica(volumeName string, replicaName string) (v20201231.Replica, error) {
	path := c.url("/volumes/" + volumeName + "/replica/" + replicaName)

	var fault v20201231.Fault
	replica := v20201231.Replica{}

	response, err := c.get(path)

	if err != nil {
		return replica, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &replica)
		return replica, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return replica, &fault
	}

	return replica, err
}

// SetPrimary - get a specific volume
func (c *Client) SetPrimary(volume string, primaryReplica string) (v20201231.Replica, error) {
	path := c.url("/volumes/" + volume + "/replicas/" + primaryReplica + "/primary")

	var fault v20201231.Fault
	replica := v20201231.Replica{}

	response, err := c.put(path)

	if err != nil {
		return replica, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &replica)
		return replica, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return replica, &fault
	}

	return replica, err
}

// CreateVolume - creates a new volume
func (c *Client) CreateVolume(create v20201231.VolumeCreate) (v20201231.Volume, error) {
	path := c.url("/volumes")

	var fault v20201231.Fault
	volume := v20201231.Volume{}

	response, err := c.post(path, create)

	if err != nil {
		return volume, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &volume)
		return volume, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return volume, &fault
	}

	return volume, err
}

func (c *Client) CreateReplica(volumeName string, create v20201231.ReplicaCreate) (v20201231.Replica, error) {
	path := c.url("/volumes/" + volumeName + "/replicas")

	var fault v20201231.Fault
	replica := v20201231.Replica{}

	response, err := c.post(path, create)

	if err != nil {
		return replica, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &replica)
		return replica, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return replica, &fault
	}

	return replica, err
}

// DeleteVolume - deletes a volume
func (c *Client) DeleteVolume(name string) (v20201231.Volume, error) {
	path := c.url("/volumes/" + name)

	var fault v20201231.Fault
	volume := v20201231.Volume{}

	response, err := c.delete(path)

	if err != nil {
		return volume, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &volume)
		return volume, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return volume, &fault
	}

	return volume, err
}

// GetSystem - get the Replix service version
func (c *Client) GetSystem(query ...string) (v20201231.System, error) {
	path := c.url("/internal/system")

	var fault v20201231.Fault
	system := v20201231.System{}

	response, err := c.get(path, query...)

	if err != nil {
		return system, err
	}

	body := response.Body()
	if response.IsSuccess() {
		err = json.Unmarshal(body, &system)
		return system, err
	}

	if err = json.Unmarshal(body, &fault); err == nil {
		return system, &fault
	}

	return system, err
}
