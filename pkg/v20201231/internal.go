package v20201231

import (
	"github.com/google/uuid"
)

// Version - reports Replix service version
type Version struct {
	Release   string   `json:"release"`
	Name      string   `json:"name"`
	Version   string   `json:"version"`
	Build     string   `json:"build"`
	Profile   string   `json:"profile"`
	Allocator string   `json:"allocator"`
	Branch    string   `json:"branch"`
	Tag       string   `json:"tag"`
	Commit    string   `json:"commit"`
	Extra     string   `json:"extra"`
	Metadata  []string `json:"metadata"`
}

// System - internal system information
type System struct {
	ID                uuid.UUID `json:"id"`
	InMaintenanceMode bool      `json:"inMaintenance"`
	Console           string    `json:"console"`
	Status            string    `json:"status"`
	Version           Version   `json:"version"`
}
