package v20201231

import (
	"encoding/json"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestRealm(t *testing.T) {
	text := []byte(`
{
  "realms": [
    {
      "name": "azure",
      "created": "2021-01-05T08:54:03.900Z",
      "azure": {
        "client_id": "ea52a75e-66cf-489f-b583-179319330019",
        "tenant_id": "810a0f8d-2707-48e1-adc0-445e112b9af5",
        "subscription_id": "a4196105-b2d7-4e6e-8855-34b4e68628be"
      }
    },
    {
      "name": "aws",
      "created": "2021-01-05T08:54:03.810Z",
      "aws": {
        "role_arn": "arn:aws:iam::534414359901:role/ReplixCrossAccountRole-BigBrother-ReplixRole-SA3YYB64AE9U"
      }
    }
  ]
}
`)
	var realms Realms

	err := json.Unmarshal(text, &realms)

	assert.Nil(t, err)

	// fmt.Printf("%#+v", realms)

	for _, realm := range realms.Realms {
		if realm.Name == "aws" && !realm.IsAws() {
			t.Errorf("Invalid AWS realm %+v", realm)
		}
		if realm.Name == "azure" && !realm.IsAzure() {
			t.Errorf("Invalid Azure realm %+v", realm)
		}
	}
}

func TestSystem(t *testing.T) {

	text := []byte(`
{
  "id": "0bcd68e6-5d65-4cff-9cb5-31d0813a5d32",
  "subscription": null,
  "inMaintenance": false,
  "console": null,
  "status": "online",
  "version": {
    "release": "20.11",
    "name": "Starlord",
    "version": "1.0.47+Starlord.172",
    "build": "172",
    "profile": "release",
    "allocator": "default",
    "branch": "1.0.47+Starlord.172",
    "tag": "1.0.47+Starlord.172",
    "commit": "4c676b45b20c7954cdcfa82c5157103de6ab24f8",
    "extra": "Built by GitLab CI"
  }
}
`)

	var system System

	err := json.Unmarshal(text, &system)

	assert.Nil(t, err)

	assert.Equal(t, system.ID, uuid.MustParse("0bcd68e6-5d65-4cff-9cb5-31d0813a5d32"))
	assert.Equal(t, system.Status, "online")
	assert.Equal(t, system.Version.Release, "20.11")
	assert.Equal(t, system.Version.Name, "Starlord")
}
