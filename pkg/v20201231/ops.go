package v20201231

import "fmt"

func (e *Fault) Error() string {
	return e.Message
}

// String - returns a string representation
func (r RealmType) String() string {
	return string(r)
}

// IsAzure tests for this Realm being an AzureRealm
func (r *Realm) IsAzure() bool {
	return r.Aws == AwsRealm{} && r.Azure != AzureRealm{}
}

// IsAws tests for this Realm being an AwsRealm
func (r *Realm) IsAws() bool {
	return r.Aws != AwsRealm{} && r.Azure == AzureRealm{}
}

// Type reports the realm type
func (r *Realm) Type() RealmType {
	if r.IsAws() {
		return Aws
	} else if r.IsAzure() {
		return Azure
	}
	panic("never reached")
}

// Replica - access named replica in the given volume
func (v *Volume) Replica(name string) (*Replica, error) {
	for _, replica := range v.Replicas {
		if replica.Name == name {
			return &replica, nil
		}
	}
	err := fmt.Errorf("No replica '%v' in volume '%v'", name, v.Name)
	return nil, err
}

// IsAws - check if
func (r *Replica) IsAws() bool {
	return r.Aws != ReplicaAws{} && r.Azure == ReplicaAzure{}
}

// IsAzure - check if replica is Azure
func (r *Replica) IsAzure() bool {
	return r.Aws == ReplicaAws{} && r.Azure != ReplicaAzure{}
}

// Subnet - access named subnet in the given volume
func (r *Replica) Subnet() string {
	if r.IsAws() {
		return r.Aws.Subnet
	}

	if r.IsAzure() {
		return r.Azure.Subnet
	}

	return "UNREACHABLE"
}

// Realm - access realm name in the given volume
func (r *Replica) Realm() string {
	if r.IsAws() {
		return r.Aws.Realm
	}

	if r.IsAzure() {
		return r.Azure.Realm
	}

	return "UNREACHABLE"
}

// IsSteady - checks for volume is in the steady state
func (s *Status) IsSteady() bool {
	switch s.Value {
	case Online, Offline, Degraded, Error:
		return true
	default:
		return false
	}
}
