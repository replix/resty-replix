package v20201231

import (
	"github.com/google/uuid"
)

// APIVersionPrefix is used to construct correct API calls
const APIVersionPrefix = "2020-12-31"

// Realm definition
type Realm struct {
	Name    string     `json:"name"`
	Created string     `json:"created"`
	Aws     AwsRealm   `json:"aws,omitempty"`
	Azure   AzureRealm `json:"azure,omitempty"`
}

// Realms definition
type Realms struct {
	Realms []Realm `json:"realms"`
}

// AwsRealm is a AWS variant of the Realm definition
type AwsRealm struct {
	RoleARN string `json:"role_arn"`
}

// AzureRealm is a Azure variant of the Realm definition
type AzureRealm struct {
	SubscriptionID string `json:"subscription_id"`
	TenantID       string `json:"tenant_id"`
	ClientID       string `json:"client_id"`
}

// RealmType - realm type
type RealmType string

// Realm type string representation
const (
	Aws   RealmType = "aws"
	Azure RealmType = "azure"
)

// Status - Volume (or other element) status definition
type Status struct {
	Value   StatusString `json:"value"`
	Message string       `json:"message"`
}

// StatusString - volume/replica status value
type StatusString string

// StatusString constants
const (
	Uninitialized StatusString = "uninitialized"
	Provisioning  StatusString = "provisioning"
	Starting      StatusString = "starting"
	Stopping      StatusString = "stopping"
	Reconfiguring StatusString = "reconfiguring"
	Online        StatusString = "online"
	Offline       StatusString = "offline"
	Syncing       StatusString = "syncing"
	Degraded      StatusString = "degraded"
	Deleting      StatusString = "deleting"
	Deleted       StatusString = "deleted"
	Error         StatusString = "error"
)

// VolumeSourceAwsSnap - volume source definition
type VolumeSourceAwsSnap struct {
	Realm  string `json:"realm"`
	Region string `json:"region"`
	ID     string `json:"id"`
}

// VolumeSource - volume source definition
type VolumeSource struct {
	AwsSnapshot VolumeSourceAwsSnap `json:"aws_snapshot"`
}

// ReplicaAws - AWS Replica definition
type ReplicaAws struct {
	Realm  string `json:"realm"`
	Region string `json:"region"`
	Subnet string `json:"subnet"`
}

// ReplicaAzure - Azure Replica definition
type ReplicaAzure struct {
	Realm         string `json:"realm"`
	ResourceGroup string `json:"resource_group"`
	Subnet        string `json:"subnet"`
}

// Replica - Volume Replica definition
type Replica struct {
	Name        string       `json:"name"`
	Description string       `json:"description"`
	Primary     bool         `json:"primary"`
	IscsiPortal string       `json:"iscsi_portal"` // TODO: consider net.IP instead
	Status      Status       `json:"status"`
	Created     string       `json:"created"`
	Modified    string       `json:"modified"`
	Aws         ReplicaAws   `json:"aws"`
	Azure       ReplicaAzure `json:"azure"`
}

// Volume definition
type Volume struct {
	Name        string       `json:"name"`
	ID          uuid.UUID    `json:"id"`
	Description string       `json:"description"`
	SizeGb      int          `json:"size_gb"`
	Source      VolumeSource `json:"source"`
	Replicas    []Replica    `json:"replicas"`
	Iqn         string       `json:"iqn"`
	Created     string       `json:"created"`
	Modified    string       `json:"modified"`
	Status      Status       `json:"status"`
}

// ReplicaCreate - new replica specification for volume create
type ReplicaCreate struct {
	Name        string       `json:"name,omitempty"`
	Description string       `json:"description,omitempty"`
	Primary     bool         `json:"primary,omitempty"`
	Aws         ReplicaAws   `json:"aws,omitempty"`
	Azure       ReplicaAzure `json:"azure,omitempty"`
}

// VolumeCreate - creates a new volume
type VolumeCreate struct {
	Name        string          `json:"name"`
	Description string          `json:"description,omitempty"`
	Replicas    []ReplicaCreate `json:"replicas"`
	SizeGb      uint16          `json:"size_gb,omitempty"`
	Source      VolumeSource    `json:"source,omitempty"`
}

// Fault - response in case of any kind of error
type Fault struct {
	HTTPCode   int    `json:"http_code"`
	HTTPStatus string `json:"http_status"`
	Message    string `json:"message"`
}
