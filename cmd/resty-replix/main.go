package main

import (
	"errors"
	"flag"
	"net/http"
	"time"

	"github.com/golang/glog"
	"gitlab.com/replix/resty-replix/pkg/replix"
	v20201231 "gitlab.com/replix/resty-replix/pkg/v20201231"
)

var (
	apiURL        = flag.String("url", "", "")
	apiToken      = flag.String("token", "", "")
	createVolName = flag.String("vol", "", "")
	realm1        = flag.String("realm-1", "", "replica 1 realm name")
	type1         = flag.String("type-1", "", "replica 1 realm type")
	region1       = flag.String("region-1", "", "replica 1 region")
	subnet1       = flag.String("subnet-1", "", "replica 1 subnet")
	realm2        = flag.String("realm-2", "", "replica 2 realm name")
	type2         = flag.String("type-2", "", "replica 2 realm type")
	region2       = flag.String("region-2", "", "replica 2 region")
	subnet2       = flag.String("subnet-2", "", "replica 2 subnet")

	primaryReplica = flag.Int("primary", 1, "primary replica id: 1 or 2")
)

func waitVolumeDeleted(client *replix.Client, volName string) bool {
	glog.Infof("\nWait until volume %s is deleted\n", volName)
	for {
		time.Sleep(15 * time.Second)

		volume, err := client.GetVolume(volName)
		if err != nil {
			glog.Errorln(err)
			// if err.Code == http.StatusNotFound {
			// 	glog.Infoln("Volume", volName, "not found")
			// 	return true
			// }
			// some other error
			return false
		}

		// status is valid
		glog.Infoln("Volume", volName, "status:", volume.Status)
		if volume.Status.Value != v20201231.Deleting {
			glog.Infoln("Volume", volName, "not deleting")
			return false
		}
	}
}

func waitVolumeStatus(client *replix.Client, volName string, waitStatus v20201231.StatusString) bool {
	for {
		volume, err := client.GetVolume(volName)
		if err != nil {
			return false
		}
		status := volume.Status
		glog.Infoln("Volume", volName, "status:", status)
		if status.Value == waitStatus {
			break
		}
		time.Sleep(15 * time.Second)
	}
	return true
}

func gracefulVolumeDelete(client *replix.Client, volName string) bool {

	var fault *v20201231.Fault
	for {
		deletedVol, err := client.DeleteVolume(volName)
		if err == nil {
			glog.Infof("Deleted volume:\n%+v\n", deletedVol)
			break
		} else {
			if errors.As(err, &fault) {
				glog.Infoln(fault.Error())
				if fault.HTTPCode != http.StatusConflict {
					return false
				}
			}
			// Conflict, volume busy - probably becoming online
			glog.Infoln("Volume", volName, "can't be deleted now")
			time.Sleep(2 * time.Second)
			if waitOK := waitVolumeStatus(client, volName, "online"); !waitOK {
				return false
			}
		}
	}
	return true
}

func main() {

	flag.Set("alsologtostderr", "true")
	flag.Set("minloglevel", "INFO")
	flag.Set("stderrthreshold", "INFO")
	flag.Parse()
	defer glog.Flush()

	// var primary1, primary2 bool
	// var primaryReplicaName string

	// switch *primaryReplica {
	// case 1:
	// 	// primary1 = true
	// 	// primary2 = false
	// 	primaryReplicaName = *region1
	// case 2:
	// 	// primary1 = false
	// 	// primary2 = true
	// 	primaryReplicaName = *region2
	// default:
	// 	glog.Infof("Invalid primary replica number: %d (1 or 2 expected)\n", *primaryReplica)
	// 	return
	// }

	pathURL := *apiURL

	glog.Infof("\nURL: %s\n", pathURL)
	glog.Infof("API token: %s\n", *apiToken)
	glog.Infof("Volume name: %s\n", *createVolName)
	glog.Infof("[replica 1] realm: %s region: %s subnet: %s\n", *realm1, *region1, *subnet1)
	glog.Infof("[replica 2] realm: %s region: %s subnet: %s\n", *realm2, *region2, *subnet2)
	glog.Infof("primary: %d\n\n", *primaryReplica)

	client := replix.New(pathURL, *apiToken)

	// Get system information version
	system, err := client.GetSystem()

	glog.Infof("System: %+v\n", system)

	// Get realms
	glog.Infof("\nGet all realms\n")
	realms, err := client.GetRealms()

	if err != nil {
		glog.Errorln(err)
		return
	}

	glog.Infof("Realms: %+v\n", realms)

	r1found := false
	r2found := false
	for _, r := range realms {
		if r.Name == *realm1 && r.Type().String() == *type1 {
			r1found = true
			glog.Infof("\nrealm 1: %s:%s found\n", *type1, *realm1)
		}
		if r.Name == *realm2 && r.Type().String() == *type2 {
			r2found = true
			glog.Infof("\nrealm 2: %s:%s found\n", *type2, *realm2)
		}
	}
	if !(r1found && r2found) {
		if !r1found {
			glog.Infof("Did not find realm 1: %s:%s\n", *type1, *realm1)
		}
		if !r2found {
			glog.Infof("Did not find realm 2: %s:%s\n", *type2, *realm2)
		}
		return
	}

	// Get volumes
	glog.Infof("\nGet all volumes\n")
	volumes, err := client.GetVolumes()
	if err != nil {
		glog.Errorln(err)
		return
	}
	glog.Infof("Volumes: %+v\n", volumes)

	newVolName := *createVolName
	volExists := false

	// Get specific volume
	for _, volume := range volumes {
		glog.Infof("\nGet volume %s\n", volume.Name)
		glog.Infof("%+v\n", volume)
		if volume.Name == newVolName {
			volExists = true
		}
	}

	if volExists {
		glog.Infof("\nvolume: %s already exists, delete it now...\n", newVolName)

		// delVolForced := replixrestapi.DeleteVolume{
		// 	Name:         newVolName,
		// 	DeleteNative: true,
		// 	Force:        true,
		// }

		// if delReqOK := gracefulVolumeDelete(client, newVolName, &delVolForced); !delReqOK {
		// 	return
		// }

		// if deleteOK := waitVolumeDeleted(client, newVolName); !deleteOK {
		// 	return
		// }
	}

	// Create volume request descriptor
	// glog.Infof("\nCreate volume %s\n", newVolName)

	// newVol := replixrestapi.CreateVolume{
	// 	Name:        newVolName,
	// 	SizeGb:      100,
	// 	Description: "auto-provisioned test volume, inst:1",
	// 	Replicas: []replixrestapi.CreateVolumeReplica{
	// 		{
	// 			Name:        *region1,
	// 			Description: newVolName + " replica 1 in " + *region1,
	// 			Realm:       *realm1,
	// 			Region:      *region1,
	// 			Subnet:      *subnet1,
	// 			IsPrimary:   primary1,
	// 		},
	// 		{
	// 			Name:        *region2,
	// 			Description: newVolName + " replica 2 in " + *region2,
	// 			Realm:       *realm2,
	// 			Region:      *region2,
	// 			Subnet:      *subnet2,
	// 			IsPrimary:   primary2,
	// 		},
	// 	},
	// }

	// createdVol, resp, err := client.CreateVolume(&newVol)
	// printResponse(resp, err)
	// if err == nil {
	// 	glog.Infof("Created volume:\n%+v\n", *createdVol)
	// } else {
	// 	if resp == nil {
	// 		return
	// 	}
	// 	glog.Infoln(resp.Message)
	// 	if resp.Code != 409 { // Conflict, already exists - continue
	// 		return
	// 	}
	// }

	// if createOK := waitVolumeStatus(client, newVolName, "online"); !createOK {
	// 	return
	// }

	// // Set primary volume replica
	// glog.Infof("\nSet primary replica %d: %s of volume %s\n",
	// 	*primaryReplica, primaryReplicaName, newVolName)

	// setPrimary := replixrestapi.VolumeReplicaID{
	// 	VolumeName:  newVolName,
	// 	ReplicaName: primaryReplicaName,
	// }

	// changedVol, resp, err := client.SetPrimaryVolumeReplica(&setPrimary)
	// printResponse(resp, err)
	// if err != nil {
	// 	if resp != nil {
	// 		glog.Infoln(resp.Message)
	// 	}
	// 	return
	// }
	// glog.Infof("Volume after setPrimary:\n%+v\n", *changedVol)

	// for {
	// 	time.Sleep(15 * time.Second)
	// 	volStatus, replicaStatus, isPrimary, resp, err := client.GetReplicaStatus(&setPrimary)
	// 	if err != nil {
	// 		if resp != nil {
	// 			glog.Infoln(resp.Message)
	// 		}
	// 		return
	// 	}
	// 	glog.Infoln("Volume", setPrimary.VolumeName, "status:", volStatus,
	// 		"replica:", setPrimary.ReplicaName, "status:", replicaStatus,
	// 		"primary:", isPrimary)
	// 	if strings.EqualFold(volStatus, "online") && strings.EqualFold(replicaStatus, "online") && isPrimary {
	// 		break
	// 	}
	// }

	// if waitOK := waitVolumeStatus(client, newVolName, "online"); !waitOK {
	// 	return
	// }

	// // Delete volume
	// glog.Infof("\nDelete volume %s\n", newVolName)

	// delVol := replixrestapi.DeleteVolume{
	// 	Name:         newVolName,
	// 	DeleteNative: true,
	// }

	// if delReqOK := gracefulVolumeDelete(client, newVolName, &delVol); !delReqOK {
	// 	return
	// }

	// if deleteOK := waitVolumeDeleted(client, newVolName); !deleteOK {
	// 	return
	// }
}
